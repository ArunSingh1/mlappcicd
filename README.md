**CI CD implementation of a ML WebApp using Gitlab, Gitlab Runner, AWS**

WEBAPP- A webpage used for Classification the Species of IRIS Flower based on the input lenghts of sepals and petals using Machine learning.

**Folder Structure**
appcicd
	- static
		- style.css
	- templates
		- index.html
	- venv
	- app.py
	- model.py
	- iris.csv
	- model.pkl
	- wsgi.py
	- gunicorn.sh
	- Dockerfile
	- requirements.txt
	- docker-compose.yaml
	- .gitlab-ci.yaml

static and templates folders have HTML CSS for the webapp. 
api.py - python file for running the api and predicting the output
model.py - python file for the ML model creation using Sklearn paNDAS numpy and pickle
model.pkl - saved ML model (random forest model)
iris.csv - labled dataset
wsgi.py - for gunicorn
gunicorn.sh - bash script for running gunicorn
Dockerfile -  Docker file for building image
requirements.txt - required packages
docker-compose.yaml - Dockerfile for running multiple images(not used) 
.gitlab-ci.yaml - gitlab file that specifies the stages of ouR CI CD Pipleline(build deploy)

Gitlab runner triggers the CI CD pipeline everytime a new code is pushed into gitlab repo, basically for every commit image gets build and deployed.

